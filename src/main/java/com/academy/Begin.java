package com.academy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

/**
 * 隔壁老王在练腰，你的情人在磨刀，
 * 加油 , 望有朝一日，走路生风，跨步起云，踏浪无痕。
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class Begin {
    public static void main(String[] args) {
        SpringApplication.run(Begin.class,args);
    }
}
