package com.academy.utils;

import java.lang.reflect.Field;

/**
 * 判断一个对象是否为null
 */
public class ObjectIsNullUtils {

    public static boolean objectIsNull(Object object) {
        Class clazz = (Class) object.getClass();

        Field[] fields = clazz.getDeclaredFields();
        boolean flag = true; //定义返回结果，默认为true
        for (Field field : fields) {
            field.setAccessible(true);
            Object fieldValue = null;
            try {
                fieldValue = field.get(object); //得到属性值
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (fieldValue != null & fieldValue.toString().length()!=0 ) {  //只要有一个属性值不为null 就返回false 表示对象不为null
                flag = false;
                break;
            }
        }
        return flag;
    }

}

