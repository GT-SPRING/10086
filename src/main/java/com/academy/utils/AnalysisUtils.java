package com.academy.utils;

import com.academy.bean.ResultError;
import org.apache.commons.lang.StringUtils;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.regex.Pattern;

/**
 * 验证图片是否符合规则
 * 不符合返回的是提示
 * 符合返回的是文件的base64的流
 */
public class AnalysisUtils {
    public static String verificationPhoto(String imgPath, Integer imageType) throws IOException {
        String imageencodeBase64 = null;
        URL url = null;
        InputStream inputStream = null;
        ByteArrayOutputStream baos = null;
        Boolean bool = false;
        if (imageType == 2) {
            // 验证是不是符合要求的字符串
            String base64Pattern = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
            // 判断和截取字符串
            if (imgPath.startsWith("data:image/jpeg;base64,")) {
                int i = imgPath.indexOf(",");
                imgPath = imgPath.substring(i+1);
            }
            boolean b = Pattern.matches(base64Pattern, imgPath);
            if (b) {
                imageencodeBase64 = imgPath;
                return imageencodeBase64;
            } else {
                return "base64不合法";
            }
        } else if (imageType == 1) {
            String[] FILETYPES = new String[]{
                    ".jpg", ".bmp", ".jpeg", ".png", ".jp2", ".jpe", ".pbm", ".pgm", ".ppm", ".tiff", ".tif",
                    ".JPG", ".BMP", ".JPEG", ".PNG", ".JP2", ".JPE", ".PDM", ".PGM", ".PPM", ".TIFF", ".TIF"
            };
            try {
                if (!StringUtils.isBlank(imgPath)) {
                    for (int i = 0; i < FILETYPES.length; i++) {
                        String fileType = FILETYPES[i];
                        if(imgPath.startsWith("http:")|| imgPath.startsWith("https:")){
                            if (imgPath.endsWith(fileType)) {
                                // 可以写日志 表示图片的路径是满足要求的
                                bool = true;
                                break;
                            }
                            continue;
                        }else {
                            return "不是网络请求";
                        }
                    }
                }
                if (bool) {
                    url = new URL(imgPath);
                    inputStream = url.openStream();
                    baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int len = 0;
                    while ((len = inputStream.read(buffer)) != -1) {
                        baos.write(buffer, 0, len);
                    }
                    BASE64Encoder decoder = new BASE64Encoder();
                    imageencodeBase64 = decoder.encode(baos.toByteArray());
                    return imageencodeBase64;
                } else {
                    return "图片后缀不符合要求";
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (baos != null) {
                    try {
                        baos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return imageencodeBase64;
    }

public static Object imageEncodeBase64Analysis(String imageEncodeBase64){
        if (imageEncodeBase64.startsWith("604")|| imageEncodeBase64.startsWith("600")){
            return  ResultError.error(ErrorEnum.AliYUNERROR03.code, ErrorEnum.AliYUNERROR03.message);
        }else if (imageEncodeBase64.startsWith("603")){
            return ResultError.error(ErrorEnum.FARMATERROR.code, ErrorEnum.FARMATERROR.message);
        }else if (imageEncodeBase64.startsWith("602")){
            return ResultError.error(ErrorEnum.BASE64ERROR15.code, ErrorEnum.BASE64ERROR15.message);
        }else if (imageEncodeBase64.startsWith("605")){
            return ResultError.error(ErrorEnum.AliYUNERROR03.code, ErrorEnum.AliYUNERROR03.message);
        }else {
            return ResultError.error(ErrorEnum.OTHERERROR.code, ErrorEnum.OTHERERROR.message);
    }
    }
    public static Object statuAnalysis(int statu) {
        if (statu == 400) {
            return ResultError.error(ErrorEnum.AliYUNERROR03.code, ErrorEnum.AliYUNERROR03.message);
        } else if (statu == 403) {
            return ResultError.error(ErrorEnum.AliYUNERROR04.code, ErrorEnum.AliYUNERROR04.message);
        } else if (statu == 408 || statu == 502 || statu == 503) {
            return ResultError.error(ErrorEnum.AliYUNERROR05.code, ErrorEnum.AliYUNERROR05.message);
        } else if (statu == 413) {
            return ResultError.error(ErrorEnum.AliYUNERROR06.code, ErrorEnum.AliYUNERROR06.message);
        } else if (statu == 450) {
            return ResultError.error(ErrorEnum.AliYUNERROR07.code, ErrorEnum.AliYUNERROR07.message);
        } else if (statu == 460 || statu == 461) {
            return ResultError.error(ErrorEnum.AliYUNERROR08.code, ErrorEnum.AliYUNERROR08.message);
        } else if (statu == 462) {
            return ResultError.error(ErrorEnum.AliYUNERROR09.code, ErrorEnum.AliYUNERROR09.message);
        } else if (statu == 464) {
            return ResultError.error(ErrorEnum.AliYUNERROR10.code, ErrorEnum.AliYUNERROR10.message);
        } else if (statu == 469) {
            return ResultError.error(ErrorEnum.AliYUNERROR11.code, ErrorEnum.AliYUNERROR11.message);
        } else if (statu == 463) {
            return ResultError.error(ErrorEnum.AliYUNERROR12.code, ErrorEnum.AliYUNERROR12.message);
        } else {
            return ResultError.error(ErrorEnum.OTHERERROR.code, ErrorEnum.OTHERERROR.message);
        }
    }
}
