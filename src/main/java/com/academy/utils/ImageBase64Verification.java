package com.academy.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;

/**
 * <p>
 * 对ImageBase64产生的文件进行验证
 */
@Slf4j
public class ImageBase64Verification {
    public static String image64Verification(String imageBase64,Integer imageType ) {
        try {
            imageBase64 = AnalysisUtils.verificationPhoto(imageBase64, imageType);
            if (StringUtils.isEmpty(imageBase64)) {
                log.error("imageBase64为空");
                imageBase64 ="604-imageBase64为空";
            } else if (imageBase64.equals("图片后缀不符合要求")) {
                log.error("图片后缀不符合要求");
                imageBase64 ="603-图片后缀不符合要求";
            }else if (imageBase64.equals("base64不合法")) {
                log.error("base64不合法");
                imageBase64 ="602-base64不合法";
            } else if (imageBase64.equals("图片规格不符合要求")) {
                log.error("图片规格不符合要求");
                imageBase64 ="600-图片规格不符合要求";
            } else if (StringUtils.isEmpty(imageBase64)) {
                log.error("图片转base64编码失败");
                imageBase64 ="601-图片转base64编码失败";
            }else if (imageBase64.equals("不是网络请求")){
                log.error("不是网络请求");
                imageBase64 ="605-图片规格不符合要求";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageBase64;
    }
}
