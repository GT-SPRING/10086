package com.academy.utils;


/**
 * 隔壁老王在练腰，你的情人在磨刀，
 * 加油 , 望有朝一日，走路生风，跨步起云，踏浪无痕。
 */
public enum ErrorEnum {
    SUCCECD("200", "成功"),
    PARAMERROR("10001", "参数错误"),
    FARMATERROR("10002", "图片格式不规范"),
    AliYUNERROR03("10003", "URL格式不规范"),
    AliYUNERROR04("10004", "没有购买，或者购买次数用尽"),
    AliYUNERROR05("10005", "阿里云识别接口链接超时"),
    AliYUNERROR06("10006", "请求阿里云body太大"),
    AliYUNERROR07("10007", "后端服务队列满，请求被拒绝，重试即可"),
    AliYUNERROR08("10008", "上传的body不符合json格式要求，是非法json"),
    AliYUNERROR09("10009", "从URL下载图像失败,或者Base64编码不合格"),
    AliYUNERROR10("10010", "阿里云OCR识别识别失败"),
    AliYUNERROR11("10011", "阿里云内部异常"),
    AliYUNERROR12("10012", "输入图像不是对应服务的图像，如行驶证服务请求的不是行驶证"),
    OTHERERROR("10013", "其他错误"),
    BASE64ERROR15("10015","上传的base64编码不符合规范"),
    DIPLOMAERROE("10014", "毕业证识别接口只支持JPG/PNG/BMP格式");
    String code;
    String message;

    ErrorEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ErrorEnum{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
