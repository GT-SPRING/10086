package com.academy.controller;

import com.academy.bean.ResultError;
import com.academy.service.CarNumberAnalysisService;
import com.academy.utils.ErrorEnum;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 车牌识别的接口
 */
@Controller
@Slf4j
public class CarNumberAnalysisController {
    @Autowired
    private CarNumberAnalysisService carNumberAnalysisService;

    /**
     * @param jsonObject 图片网络路径
     */
    @ResponseBody
    @RequestMapping("carNoRec")
    public Object carNumberUpload(@RequestBody JSONObject jsonObject) {
        String imageUrl = jsonObject.getString("imageUrl");
        Integer imageType=jsonObject.getInteger("imageType");
        if (imageUrl.isEmpty() & imageType == null) {
            log.error("参数不能为null");
            return  ResultError.error(ErrorEnum.PARAMERROR.getCode(), ErrorEnum.PARAMERROR.getMessage());
        }
        Object carNumber = carNumberAnalysisService.getCarNumber(imageUrl,imageType);
        return carNumber;
    }
}
