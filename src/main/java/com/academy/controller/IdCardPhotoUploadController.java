package com.academy.controller;

import com.academy.bean.ResultError;
import com.academy.service.IdCardPhotoUploadService;
import com.academy.utils.ErrorEnum;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
public class IdCardPhotoUploadController {

    @Autowired
    private IdCardPhotoUploadService idCardPhotoUploadService;

    /**
     * 身份证识别的接口
     *
     * @param   jsonObject 上传的图片url
     * @imageConfig 正面/反面:face/back     1/2
     */
    @ResponseBody
    @RequestMapping(value = "/identifyCardRec", method = RequestMethod.POST)
    public Object idCardFace(@RequestBody JSONObject jsonObject) {

        String imageUrl = jsonObject.getString("imageUrl");
        String imageConfig = jsonObject.getString("imageConfig");
        Integer imageType=jsonObject.getInteger("imageType");
        if (StringUtils.isEmpty(imageUrl) || imageConfig == null ||imageType==null ) {
            log.error("参数为空" + ErrorEnum.PARAMERROR.toString());
            return  ResultError.error(ErrorEnum.PARAMERROR.getCode(), ErrorEnum.PARAMERROR.getMessage());
        }
        Object idCar = idCardPhotoUploadService.getIdCar(imageUrl, Integer.valueOf(imageConfig), imageType);
        return idCar;
    }
}
