package com.academy.controller;

import com.academy.bean.ResultError;
import com.academy.service.DiplomaAnalysisService;
import com.academy.utils.ErrorEnum;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 识别 毕业证照片的controller
 */
@Controller
@Slf4j
public class DiplomaAnalysisController {
    @Autowired
    private DiplomaAnalysisService diplomaAnalysisService;

    /**
     * @param jsonObject 图片上传的路径 json格式
     */
    @ResponseBody
    @RequestMapping("/graduationCertRec")
    public Object diplomaUpload(@RequestBody JSONObject jsonObject) {
        String[] FILETYPES = new String[]{
                ".jpg", ".bmp", ".png",
                ".JPG", ".BMP", ".PNG"
        };
        String imageUrl = jsonObject.getString("imageUrl");
        Integer imageType = jsonObject.getInteger("imageType");
        if (StringUtils.isEmpty(imageUrl) || imageType == null) {
            log.error("参数为空" + ErrorEnum.PARAMERROR.toString());
            return  ResultError.error(ErrorEnum.PARAMERROR.getCode(), ErrorEnum.PARAMERROR.getMessage());
        }
        if (!StringUtils.isBlank(imageUrl) & imageType== 1 ) {
            for (int i = 0; i < FILETYPES.length; i++) {
                String fileType = FILETYPES[i];
                if (imageUrl.endsWith(fileType)) {
                    // 可以写日志 表示图片的路径是满足要求的
                    return diplomaAnalysisService.getDiploma(imageUrl, imageType);
                }else {
                    continue;
                }
            }
            return ResultError.error(ErrorEnum.DIPLOMAERROE.getCode(),ErrorEnum.DIPLOMAERROE.getMessage());
        }
        if (imageType == 2 & imageUrl.startsWith("data") || imageUrl.startsWith("/9") ){
            return diplomaAnalysisService.getDiploma(imageUrl, imageType);
        }else {
            return ResultError.error(ErrorEnum.BASE64ERROR15.getCode(),ErrorEnum.BASE64ERROR15.getMessage());
        }
    }
}