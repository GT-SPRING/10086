package com.academy.bean;


public class Diploma   {
    private String number;
    private String name;
    private String major;
    private String sex;
    private String birth;
    private String agency;
    private String type;

    public Diploma() {
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getMajor() {
        return major;
    }

    public String getSex() {
        return sex;
    }

    public String getBirth() {
        return birth;
    }

    public String getAgency() {
        return agency;
    }

    public String getType() {
        return type;
    }
}
