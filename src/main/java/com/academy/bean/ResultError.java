package com.academy.bean;

import java.io.Serializable;

/**
 * 自定义返回的异常信息
 */
public class ResultError<T> implements Serializable {
    private String code;
    private String message;
    public ResultError(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ResultError error(String code, String message) {
        ResultError resultError = new ResultError(code,message);
        return resultError;
    }
    public ResultError() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
