package com.academy.bean;


public class IdCard   {
    // 身份证图片配置：1 正面，2 反面
    private int imageConfig;
    // 地址，正面时返回
    private String address;
    //name 姓名 正面时返回
    private String name;
    //nationality 民族 正面时返回
    private String nationality;
    // 性别 正面时返回
    private String sex;
    // 出生年月 正面时返回
    private String birth;
    //身份证号 正面时返回
    private String number;
    // 签发机关 反面时返回
    private String issue;
    // 有效起始时间 反面时返回
    private String startDate;
    // 有效结束时间 反面时返回
    private String endDate;

    public IdCard() { }


    public int getImageConfig() {
        return imageConfig;
    }

    public void setImageConfig(int imageConfig) {
        this.imageConfig = imageConfig;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
