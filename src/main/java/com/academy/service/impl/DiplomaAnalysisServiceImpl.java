package com.academy.service.impl;

import com.academy.bean.Diploma;
import com.academy.bean.ResultError;
import com.academy.bean.ResultSuccess;
import com.academy.service.DiplomaAnalysisService;
import com.academy.utils.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class DiplomaAnalysisServiceImpl implements DiplomaAnalysisService {
    @Value("${diplomaHost}")
    private String host;
    @Value("${diplomaPath}")
    private String path;
    @Value("${diplomaMethod}")
    private String method;
    @Value("${diplomaAppcode}")
    private String appcode;

    @Override
    public Object getDiploma(String imagePath, Integer imageType) {
        String imageEncodeBase64 = null;
        Diploma diploma = new Diploma();
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String UUID = df.format(new Date());
        headers.put("X-Ca-Nonce", UUID);
        if (imageType == 1 || imageType == 2) {
            imageEncodeBase64 = ImageBase64Verification.image64Verification(imagePath, imageType);
        } else {
            return ResultError.error(ErrorEnum.PARAMERROR.getCode(), ErrorEnum.PARAMERROR.getMessage());
        }
        if (imageEncodeBase64.startsWith("60")) {
            return AnalysisUtils.imageEncodeBase64Analysis(imageEncodeBase64);
        }
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        Map<String, String> querys = new HashMap();
        Map<String, String> bodys = new HashMap();
        bodys.put("IMAGE", imageEncodeBase64);
        bodys.put("IMAGE_TYPE", "0");
        try {
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            String jsonObject = EntityUtils.toString(response.getEntity());
            int stat = response.getStatusLine().getStatusCode();
            if (stat != 200) {
                log.error("阿里云返回的错误数据信息:code=" + stat + "header-msg" + response.getFirstHeader("X-Ca-Error-Message") + "body error msg" + EntityUtils.toString(response.getEntity()));
                return AnalysisUtils.statuAnalysis(stat);
            }
            log.info(jsonObject);
            JSONObject object = JSON.parseObject(jsonObject);
            diploma.setNumber(object.getString("GRADUCATION_ID"));
            diploma.setName(object.getString("GRADUCATION_OWNER_NAME"));
            diploma.setMajor(object.getString("GRADUCATION_MAJOR"));
            diploma.setSex(object.getString("GRADUCATION_OWNER_GENDER"));
            diploma.setBirth(object.getString("GRADUCATION_OWNER_BIRTH"));
            diploma.setAgency(object.getString("GRADUCATION_AUTHORIZED_AGENCY"));
            diploma.setType(object.getString("GRADUCATION_TYPE"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean b = ObjectIsNullUtils.objectIsNull(diploma);
        if (b) {
            return ResultError.error(ErrorEnum.AliYUNERROR12.getCode(), ErrorEnum.AliYUNERROR12.getMessage());
        }
        return new ResultSuccess<>(ErrorEnum.SUCCECD.getCode(), ErrorEnum.SUCCECD.getMessage(), diploma);
    }
}
