package com.academy.service.impl;

import com.academy.bean.IdCard;
import com.academy.bean.ResultError;
import com.academy.bean.ResultSuccess;
import com.academy.service.IdCardPhotoUploadService;
import com.academy.utils.AnalysisUtils;
import com.academy.utils.ErrorEnum;
import com.academy.utils.HttpUtils;
import com.academy.utils.ImageBase64Verification;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 隔壁老王在练腰，你的情人在磨刀，
 * 加油 , 望有朝一日，走路生风，跨步起云，踏浪无痕。
 */
@Service
@Slf4j
public class IdCardPhotoUploadServiceImpl implements IdCardPhotoUploadService {
    @Value("${idCardHost}")
    private String host;

    @Value("${idCardPath}")
    private String path;

    @Value("${idCardMethod}")
    private String method;

    @Value("${idCardAppcode}")
    private String appcode;

    @Override
    public Object getIdCar(String imagePath, Integer orientation, Integer imageType) {
        String imageEncodeBase64 = null;
        IdCard idCard = new IdCard();
        if (imageType == 1 || imageType == 2) {
            imageEncodeBase64 = ImageBase64Verification.image64Verification(imagePath, imageType);
        } else {
            return  ResultError.error(ErrorEnum.PARAMERROR.getCode(), ErrorEnum.PARAMERROR.getCode());
        }
        if (imageEncodeBase64.startsWith("60")){
            return AnalysisUtils.imageEncodeBase64Analysis(imageEncodeBase64);
        }
        try {
            JSONObject configObj = new JSONObject();
            if (orientation == 1) {
                configObj.put("side", "face");
            } else if (orientation == 2) {
                configObj.put("side", "back");
            }
            String config_str = configObj.toString();
            // 拼装参数
            JSONObject requestObj = new JSONObject();
            requestObj.put("image", imageEncodeBase64);
            if (config_str.length() > 0) {
                requestObj.put("configure", config_str);
            }
            Map<String, String> querys = new HashMap();
            String bodys = requestObj.toString();
            Map<String, String> headers = new HashMap();
            headers.put("Authorization", "APPCODE " + appcode);
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            int stat = response.getStatusLine().getStatusCode();
            if (stat != 200) {
                log.error("阿里云返回的错误数据信息:code=" + stat + "header-msg" + response.getFirstHeader("X-Ca-Error-Message") + "body error msg" + EntityUtils.toString(response.getEntity()));
                return AnalysisUtils.statuAnalysis(stat);
            }
            String res = EntityUtils.toString(response.getEntity());
            log.info(res);
            JSONObject res_obj = JSON.parseObject(res);
            // 表示正面
            if (orientation == 1) {
                idCard.setImageConfig(1);
                // 拼接假数据  测试并发
//                idCard.setAddress("测试并发的假地址");
//                idCard.setName("测试并发的假数据");
//                idCard.setNationality("测试并发的假数据");
//                idCard.setSex("测试并发的假数据");
//                idCard.setBirth("测试并发的假数据");
//                idCard.setNumber("测试并发的假数据");
                // 真的数据
                idCard.setAddress(res_obj.getString("address"));
                idCard.setName(res_obj.getString("name"));
                idCard.setNationality(res_obj.getString("nationality"));
                idCard.setSex(res_obj.getString("sex"));
                idCard.setBirth((res_obj.getString("birth")));
                idCard.setNumber(res_obj.getString("num"));
                idCard.setEndDate("");
                idCard.setStartDate("");
                idCard.setIssue("");
            } else if (orientation == 2) {
                // 表示反面
                idCard.setImageConfig(2);
                idCard.setAddress("");
                idCard.setName("");
                idCard.setNationality("");
                idCard.setSex("");
                idCard.setBirth("");
                idCard.setNumber("");
//                // 测试假数据
//                idCard.setEndDate("测试并发的假数据");
//                idCard.setIssue("测试并发的假数据");
//                idCard.setStartDate("测试并发的假数据");
                idCard.setEndDate(res_obj.getString("end_date"));
                idCard.setIssue(res_obj.getString("issue"));
                idCard.setStartDate(res_obj.getString("start_date"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResultSuccess<>(ErrorEnum.SUCCECD.getCode(),ErrorEnum.SUCCECD.getMessage(),idCard);
    }
}
