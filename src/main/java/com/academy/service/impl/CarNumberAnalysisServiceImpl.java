package com.academy.service.impl;

import com.academy.bean.CarNumber;
import com.academy.bean.ResultError;
import com.academy.bean.ResultSuccess;
import com.academy.service.CarNumberAnalysisService;
import com.academy.utils.AnalysisUtils;
import com.academy.utils.ErrorEnum;
import com.academy.utils.HttpUtils;
import com.academy.utils.ImageBase64Verification;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 隔壁老王在练腰，你的情人在磨刀，
 * 加油 , 望有朝一日，走路生风，跨步起云，踏浪无痕。
 */
@Service
@Slf4j
public class CarNumberAnalysisServiceImpl implements CarNumberAnalysisService {

    @Value("${carNumberMultiple}")
    private Boolean multiple;
    @Value("${carNumberHost}")
    private String host;
    @Value("${carNumberPath}")
    private String path;
    @Value("${carNumberMethod}")
    private String method;
    @Value("${carNumberAppcode}")
    private String appcode;

    @Override
    public Object getCarNumber(String imagePath,Integer imageType) {
        CarNumber carNumber = new CarNumber();
        String imageEncodeBase64 = null;
        if (imageType == 1 || imageType == 2 ){
            imageEncodeBase64 = ImageBase64Verification.image64Verification(imagePath,imageType);
        }else{
            return  ResultError.error(ErrorEnum.PARAMERROR.getCode(),ErrorEnum.PARAMERROR.getCode());
        }
        if (imageEncodeBase64.startsWith("60")){
            return AnalysisUtils.imageEncodeBase64Analysis(imageEncodeBase64);
        }
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        JSONObject configObj = new JSONObject();
        configObj.put("multi_crop", multiple);
        String config_str = configObj.toString();
        Map<String, String> querys = new HashMap<String, String>();
        JSONObject requestObj = new JSONObject();
        requestObj.put("image", imageEncodeBase64);
        if (config_str.length() > 0) {
            requestObj.put("configure", config_str);
        }
        String bodys = requestObj.toString();
        try {
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            int stat = response.getStatusLine().getStatusCode();
            if (stat != 200) {
                log.error("阿里云返回的错误数据信息:code=" + stat + "header-msg" + response.getFirstHeader("X-Ca-Error-Message") + "body error msg" + EntityUtils.toString(response.getEntity()));
                return AnalysisUtils.statuAnalysis(stat);
            }
            String res = EntityUtils.toString(response.getEntity());
            JSONObject res_obj = JSON.parseObject(res);
            String jsonObj = res_obj.getJSONArray("plates").getString(0);
            JSONObject jsonObject = JSON.parseObject(jsonObj);
            // 将结果写到日志内  全部的返回结果
            log.info(res_obj.toJSONString());
            carNumber.setNumber(jsonObject.getString("txt"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResultSuccess<>(ErrorEnum.SUCCECD.getCode(),ErrorEnum.SUCCECD.getMessage(),carNumber);
    }
}
