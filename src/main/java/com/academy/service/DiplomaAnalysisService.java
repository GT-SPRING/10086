package com.academy.service;

public interface DiplomaAnalysisService {
     Object getDiploma(String imagePath,Integer imageType);
}
